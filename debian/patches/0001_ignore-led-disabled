commit f47f78eb0bd9fba455f01c8c6dead3bd75242b2b
Author: Peter Hutterer <peter.hutterer@who-t.net>
Date:   Tue Dec 20 15:36:55 2016 +1000

    Ignore LED updates for disabled devices
    
    If an XKB AccessX timeout is set and a VT switch is triggered, the
    AccessXTimeoutExpire function may be called after the device has already been
    disabled. This can cause a null-pointer dereference as our shared libinput
    device may have been released by then.
    
    In the legacy drivers this would've simply caused a write to an invalid fd
    (-1), not a crash. Here we need to be more careful.
    
    https://bugs.freedesktop.org/show_bug.cgi?id=98464
    
    Signed-off-by: Peter Hutterer <peter.hutterer@who-t.net>
    Reviewed-by: Hans de Goede <hdegoede@redhat.com>

diff --git a/src/xf86libinput.c b/src/xf86libinput.c
index b130a77..fd38c3b 100644
--- a/src/xf86libinput.c
+++ b/src/xf86libinput.c
@@ -785,6 +785,9 @@ xf86libinput_kbd_ctrl(DeviceIntPtr device, KeybdCtrl *ctrl)
     struct xf86libinput *driver_data = pInfo->private;
     struct libinput_device *ldevice = driver_data->shared_device->device;
 
+    if (!device->enabled)
+	    return;
+
     while (bits[i].xbit) {
 	    if (ctrl->leds & bits[i].xbit)
 		    leds |= bits[i].code;
